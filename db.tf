resource "aws_db_subnet_group" "db_subnet_group" {
  description = "DB Subnet Group"
  subnet_ids  = [data.aws_subnet.lab_subnet_private1.id, 
                  data.aws_subnet.lab_subnet_private2.id]

  tags = {
    Name = "DB-Subnet-Group"
  }
}


resource "aws_db_instance" "lab_db" {
  allocated_storage       = 20
  db_name                 = "lab"
  engine                  = var.db_engine
  engine_version          = var.db_engine_version
  instance_class          = var.db_instance_type
  username                = var.db_user
  password                = var.db_password
  parameter_group_name    = "default.mysql5.7"

  multi_az = true
  vpc_security_group_ids  = [aws_security_group.allow_db_access.id]
  db_subnet_group_name    = aws_db_subnet_group.db_subnet_group.name
  skip_final_snapshot     = true
  backup_retention_period = 7

  tags = {
    Name = "Lab-DB"
  }
}

resource "aws_db_instance" "lab_db_replica" {
  replicate_source_db         = aws_db_instance.lab_db.identifier
  #allocated_storage           = 20
  #max_allocated_storage       = 30
  identifier                  = "lab-db-replica"
  skip_final_snapshot         = true
  instance_class          = var.db_instance_type
}