## VPC

data "aws_vpc" "lab_vpc" {
  filter {
    name = "tag:Name"
    values =  ["Lab VPC"]     
  }
}


## Subnets
data "aws_subnet" "lab_subnet_private1" {
  filter {
    name = "tag:Name"
    values = ["Private Subnet 1"]
  }
}


data "aws_subnet" "lab_subnet_private2" {
  filter {
    name  = "tag:Name"
    values = ["Private Subnet 2"]
  }
}