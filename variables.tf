variable "db_user" {
  description = "RDS user"
  type        = string
  sensitive   = true
}

variable "db_password" {
  description = "RDS user password"
  type        = string
  sensitive   = true
}

variable "db_engine_version" {
  description = "RDS Engine version"
  type        = string
  default     = "5.7"
}

variable "db_engine" {
  description = "RDS Engine"
  type        = string
  default     = "mysql"
}


variable "db_instance_type" {
  description = "RDS instance type"
  type        = string
  default     = "db.t3.micro"
}