# Lab 5: Build Your DB Server and Interact With Your DB Using an App

Module: Module 8 - Databases  (AWS Cloud Foundations)


In this laboratory, we will start with the following infrastructure:
![Initial Infrastructure](./images/initial-infrastructure.png)

At the end of the lab, this is the infrastructure:
![Final Infrastructure](./images/final-infrastructure.png)
