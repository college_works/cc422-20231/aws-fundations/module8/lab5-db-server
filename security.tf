resource "aws_security_group" "allow_db_access" {
  name        = "DB Security Group"
  description = "Permit access from Web Security Group"
  vpc_id      = data.aws_vpc.lab_vpc.id

  ingress {
    description      = "MySQL from VPC"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "DB Security Group"
  }
}